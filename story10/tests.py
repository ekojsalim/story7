import os
import time
from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from .views import profile_view, signup_view

from django.contrib.auth.models import User

# Create your tests here.
class RoutingViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_login_url_is_exist(self):
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_logout_url_is_exist(self):
        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 200)

    def test_profile_url_is_exist(self):
        response = self.client.get('/accounts/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_view_func(self):
        found = resolve('/accounts/profile/')
        self.assertEqual(found.func, profile_view)

    def test_signup_url_is_exist(self):
        response = self.client.get('/accounts/signup/')
        self.assertEqual(response.status_code, 200)

    def test_signup_using_view_func(self):
        found = resolve('/accounts/signup/')
        self.assertEqual(found.func, signup_view)

    def test_profile_shows_description(self):
        response = self.client.get('/accounts/profile/')
        self.assertContains(response, 'Your Profile Description')
        
class BooksFunctionalTestCase(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        cls.selenium = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        cls.selenium.implicitly_wait(25)

        cls.user = User.objects.create_user(username='testuser', password='12345')


    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_profile_then_login(self):
        self.selenium.get(f"{self.live_server_url}/accounts/profile")

        self.assertIn('Not Authenticated', self.selenium.page_source)

        log_button = self.selenium.find_element_by_css_selector('#btn-log')

        log_button.click()

        username = self.selenium.find_element_by_css_selector('input[name="username"]') 
        username.send_keys('testuser')

        password = self.selenium.find_element_by_css_selector('input[name="password"]') 
        password.send_keys('12345')

        self.selenium.find_element_by_css_selector('input[type="submit"]').click()

        self.assertIn('testuser', self.selenium.page_source)
