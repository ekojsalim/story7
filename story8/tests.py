import os
import time
from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from .views import accordion_view

# Create your tests here.
class RoutingViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_all_status_url_is_exist(self):
        response = self.client.get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_all_status_using_view_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, accordion_view)

class StatusFunctionalTestCase(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        cls.selenium = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        cls.selenium.implicitly_wait(25)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_accordion_up_change(self):
        self.selenium.get(f"{self.live_server_url}/story8/")
        time.sleep(5)

        second_acc_el = self.selenium.find_elements_by_css_selector('.acc-el')[1]
        second_acc_el_up_button = second_acc_el.find_element_by_css_selector('.up-button')

        second_acc_el_up_button.click()

        new_first_element = self.selenium.find_elements_by_css_selector('.acc-el')[0]
        self.assertEqual(second_acc_el.find_element_by_tag_name('p').text, new_first_element.find_element_by_tag_name('p').text)

    def test_accordion_down_change(self):
        self.selenium.get(f"{self.live_server_url}/story8/")
        time.sleep(5)

        second_acc_el = self.selenium.find_elements_by_css_selector('.acc-el')[1]
        second_acc_el_down_button = second_acc_el.find_element_by_css_selector('.down-button')

        second_acc_el_down_button.click()

        new_third_element = self.selenium.find_elements_by_css_selector('.acc-el')[2]
        self.assertEqual(second_acc_el.find_element_by_tag_name('p').text, new_third_element.find_element_by_tag_name('p').text)
    
    def test_accordion_clickable(self):
        self.selenium.get(f"{self.live_server_url}/story8/")
        time.sleep(5)

        acc_el = self.selenium.find_elements_by_css_selector('.acc-el')[1]
        acc_el_header = acc_el.find_element_by_css_selector('.message-header')

        message_body = acc_el.find_element_by_css_selector('.message-body')

        self.assertFalse(message_body.is_displayed())

        acc_el_header.click()

        time.sleep(2)

        self.assertTrue(message_body.is_displayed())

    def test_btn_theme(self):
        self.selenium.get(f"{self.live_server_url}/story8/")
        time.sleep(5) 
        
        original_bg = self.selenium.find_element_by_tag_name('html').value_of_css_property('background-color')
        original_acc_bg = self.selenium.find_element_by_css_selector('.message-header').value_of_css_property('background-color')

        self.selenium.find_element_by_id('btn-theme').click()

        new_bg = self.selenium.find_element_by_tag_name('html').value_of_css_property('background-color') 
        new_acc_bg = self.selenium.find_element_by_css_selector('.message-header').value_of_css_property('background-color')

        self.assertNotEqual(original_bg, new_bg)
        self.assertNotEqual(original_acc_bg, new_acc_bg)
