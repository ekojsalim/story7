from django.urls import path

from .views import accordion_view

urlpatterns = [
    path('', accordion_view, name='accordion')
]