from django.shortcuts import render

# Create your views here.
def accordion_view(request):
    return render(request, "story8/accordion.html")