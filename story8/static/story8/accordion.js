$(() => {
  const allMessages = $(".acc-el > .message-body").hide();

  $(".acc-el > .message-body").first().slideDown();

  $(".acc-el > .message-header").click(function () {
    allMessages.slideUp();
    $(this).parent().children(".message-body").slideDown();
    return false;
  });

  $("#btn-theme").click(toggleTheme);

  setUpDownBehavior();
});

function toggleTheme() {
  console.log("ha");
  $("html, .title").toggleClass("dark");
  $(".acc-el").toggleClass("is-info");
}

function setUpDownBehavior() {
  $(".up-button, .down-button").off("click");
  $(".up-button").click(function () {
    const current = $(this).closest(".acc-el");
    const previous = current.prev();
    current.insertBefore(previous);
    setUpDownBehavior();
  });

  $(".down-button").click(function () {
    const current = $(this).closest(".acc-el");
    const next = current.next();
    current.insertAfter(next);
    setUpDownBehavior();
  });

  $(".up-button").removeClass("has-text-grey");
  $(".down-button").removeClass("has-text-grey");

  // set the first up button to grey
  $(".up-button").first().addClass("has-text-grey");
  // and the last
  $(".down-button").last().addClass("has-text-grey");
}
