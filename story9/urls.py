from django.urls import path

from .views import books_view, books_liked_view

urlpatterns = [
    path('', books_view, name='books'),
    path('api/liked', books_liked_view, name='books-liked-view')]