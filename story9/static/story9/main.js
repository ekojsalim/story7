// local cache of book
let books = {};
let lastFetchedBooks = [];

let notyf = new Notyf();

const getBookHTMLString = ({
  book_id: id,
  image: imageURL,
  title,
  author,
  publisher,
  likes,
}) => `
    <div class="grid-item">
    <div class="card book">
        <div class="card-image">
        <figure class="image is-4by5">
            <img
            src="${imageURL}"
            alt="Placeholder image"
            />
        </figure>
        </div>
        <div class="card-content">
        <div class="media">
            <div class="media-content">
            <p class="title is-4">
                ${title}
            </p>
            <p class="subtitle is-6">${author}</p>
            </div>
        </div>

        <div class="content">
            <table class="table">
            <tr>
                <td><strong>Publisher</strong></td>
                <td>${publisher}</td>
            </tr>
            <tr>
                <td><strong>Likes</strong></td>
                <td id="${id + "-likes"}" class="book-like">${likes}</td>
            </tr>
            </table>
        </div>
        </div>
        <footer class="card-footer">
        <span class="card-footer-item card-like" onClick="handleLike('${id}')">
            <span class="icon is-small like-icon">
            <i class="fas fa-heart"></i>
            </span>
            <span>Like</span></span
        >
        </footer>
    </div>
    </div>
`;

const getBookModalHTMLString = ({
  book_id: id,
  image: imageURL,
  title,
  author,
  publisher,
  likes,
}) => `
    <div class="modal-book-item">
    <div class="card">
        <div class="card-image">
        <figure class="image is-4by5">
            <img
            src="${imageURL}"
            alt="Placeholder image"
            />
        </figure>
        </div>
        <div class="card-content">
        <div class="media">
            <div class="media-content">
            <p class="title is-4">
                ${title}
            </p>
            <p class="subtitle is-6">${author}</p>
            </div>
        </div>

        <div class="content">
            <table class="table">
            <tr>
                <td><strong>Publisher</strong></td>
                <td>${publisher}</td>
            </tr>
            <tr>
                <td><strong>Likes</strong></td>
                <td id="${id + "-likes"}">${likes}</td>
            </tr>
            </table>
        </div>
        </div>
    </div>
    </div>
`;

async function getLikedBooks() {
  startProgressBar();
  const url = "/story9/api/liked";
  const result = await (await fetch(url)).json();

  if (result.success) {
    result.books.forEach((book) => {
      books[book.book_id] = book;
    });
  }
  endProgressBar();
}

async function queryGoogleAPI(q) {
  startProgressBar();
  const url = `https://www.googleapis.com/books/v1/volumes?q=${q}&maxResults=30`;
  try {
    const result = await (await fetch(url)).json();

    if (result) {
      lastFetchedBooks = [];
      result.items.forEach((item) => {
        const { title, authors, publisher, imageLinks } = item.volumeInfo;
        const book = {
          book_id: item.id,
          title,
          author: authors ? authors.join(", ") : "Unknown",
          image: imageLinks?.thumbnail || imageLinks?.smallThumbnail,
          publisher: publisher ? publisher : "Unknown",
          likes: 0,
        };
        if (!books.hasOwnProperty(item.id)) {
          books[item.id] = book;
        } else {
          book.likes = books[item.id].likes;
        }
        lastFetchedBooks.push(book);
      });
    }

    notyf.success("Books Fetched!");
  } catch (error) {
    notyf.error("Failed to fetch books!");
  }

  endProgressBar();
}

function renderBooks() {
  $(".grid").masonry();

  // destroy masonry
  $(".grid").masonry("destroy");
  $(".grid").removeData("masonry");

  $("#book-results-container").html(
    lastFetchedBooks.map(getBookHTMLString).join("")
  );

  $(".grid").masonry({
    // options
    itemSelector: ".grid-item",
  });
}

function startProgressBar() {
  $("#progress").removeAttr("value");
}

function endProgressBar() {
  $("#progress").attr("value", 0);
}

function getCookie(n) {
  let value = "; " + document.cookie;
  let parts = value.split("; " + n + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

async function handleLike(id) {
  startProgressBar();
  const url = "/story9/api/liked";

  const book = books[id];
  book.likes++;

  // update like counts
  $(`#${id}-likes`).text(book.likes);

  const csrftoken = getCookie("csrftoken");

  const response = await fetch(url, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    headers: { "X-CSRFToken": csrftoken },
    body: JSON.stringify(book),
  });

  const result = await response.json();

  if (result.success) {
    notyf.success("Likes Added!");
  } else {
    book.likes--;
    $(`#${id}-likes`).text(book.likes);
    notyf.error("Error Adding Likes!");
  }

  endProgressBar();
}

async function initialize() {
  await getLikedBooks();
  await queryGoogleAPI("indonesia");
  console.log(lastFetchedBooks);
  renderBooks();
}

$(() => {
  initialize();

  $("#search-form").submit(async (e) => {
    e.preventDefault();
    await queryGoogleAPI($("#query").val());
    renderBooks();
  });

  $("#btn-add").on("click", async function () {
    await getLikedBooks();

    const top5 = Object.values(books)
      .sort((a, b) => b.likes - a.likes)
      .slice(0, 5)
      .filter((a) => a.likes > 0);

    $("#modal-content").html(
      `<h2 class="title">Most Liked Book</h2>` +
        top5.map(getBookModalHTMLString).join("")
    );

    $("#modal").addClass("is-active");
  });
  $("#btn-close, .modal-background, .close-modal").on("click", function () {
    $("#modal").removeClass("is-active");
  });
});
