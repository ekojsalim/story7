import os
import time
from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from .views import books_view, books_liked_view

# Create your tests here.
class RoutingViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_books_url_is_exist(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_books_using_view_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, books_view)

    def test_books_api_url_is_exist(self):
        response = self.client.get('/story9/api/liked')
        self.assertEqual(response.status_code, 200)

    def test_books_api_url_using_view_func(self):
        found = resolve('/story9/api/liked')
        self.assertEqual(found.func, books_liked_view)

class BooksFunctionalTestCase(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        cls.selenium = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        cls.selenium.implicitly_wait(25)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_book_can_be_searched(self):
        self.selenium.get(f"{self.live_server_url}/story9/")
        search_el = self.selenium.find_element_by_css_selector('#query')

        search_el.send_keys('ppw')

        button_search = self.selenium.find_element_by_css_selector('#btn-search') 

        button_search.click()

        time.sleep(5)

        self.assertIn('Hawaiki', self.selenium.page_source)
 

    def test_book_can_be_liked(self):
        self.selenium.get(f"{self.live_server_url}/story9/")
        time.sleep(3)

        first_book = self.selenium.find_elements_by_css_selector('.book')[0]
        first_book_like = first_book.find_element_by_css_selector('.book-like').text
        first_book_like_button = first_book.find_element_by_css_selector('.card-like')

        first_book_like_button.click()
        
        time.sleep(1)

        first_book_like_new = first_book.find_element_by_css_selector('.book-like').text

        self.assertNotEqual(first_book_like, first_book_like_new)
