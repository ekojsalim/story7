from django.db import models

# Create your models here.
class Book(models.Model):
    book_id = models.CharField(primary_key=True, max_length=100)
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    image = models.URLField()
    publisher = models.CharField(max_length=200)
    likes = models.PositiveIntegerField()
    