import json

from django.shortcuts import render
from django.http import JsonResponse
from django.forms.models import model_to_dict

from .models import Book

def books_view(request):
    return render(request, 'story9/books.html')

def books_liked_view(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)

            book_id = data.pop('book_id')

            book, _ = Book.objects.update_or_create(
                book_id = book_id,
                defaults = data
            )

            return JsonResponse({'success': True, 'book': model_to_dict(book)})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})
    else:
        books = Book.objects.order_by('-likes')
        return JsonResponse({'success': True, 'books': list(books.values())})


