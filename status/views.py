import random
from django.shortcuts import render, redirect

from .models import Status

def new_color(c):
    colors = ["dark", "primary", "link", "info", "success", "warning", "danger"]
    avail_colors = [col for col in colors if col != c]
    return random.choice(avail_colors)


# Create your views here.
def all_status_view(request):
    status_id = request.POST.get("id")
    if request.method == "POST" and status_id:
        status_req = Status.objects.get(pk=status_id)
        status_req.color = new_color(status_req.color)
        status_req.save()
    all_status = Status.objects.all()
    context = {
        "all_status": all_status
    }
    return render(request, "status/all.html", context)

def confirm_view(request):
    sender = request.POST.get("sender")
    content = request.POST.get("content")
    if request.method == "POST" and sender and content:
        context = {
            "sender": sender,
            "content": content
        }
        return render(request, "status/confirm.html", context)
    else:
        return redirect("/")

def handle_confirmed(request):
    sender = request.POST.get("sender")
    content = request.POST.get("content")
    if request.method == "POST" and sender and content:
        Status.objects.create(sender=sender, content=content)
    return redirect("/")