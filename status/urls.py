from django.urls import path

from .views import all_status_view, confirm_view, handle_confirmed

urlpatterns = [
    path('', all_status_view, name='all-status'),
    path('confirm', confirm_view, name='confirm'),
    path('confirmed', handle_confirmed, name='confirmed')
]