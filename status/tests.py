import os
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from .views import all_status_view, confirm_view
from .models import Status

# Create your tests here.
class RoutingViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_all_status_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_all_status_using_view_func(self):
        found = resolve('/')
        self.assertEqual(found.func, all_status_view)

    def test_confirm_url_is_exist(self):
        response = self.client.post('/confirm', {'sender': 'Haha', 'content': 'Huhu'})
        self.assertEqual(response.status_code, 200)

    def test_barang_all_using_view_func(self):
        found = resolve('/confirm')
        self.assertEqual(found.func, confirm_view)

class StatusModelTestCase(TestCase):
    def test_model_can_create_new_status(self):
        Status.objects.create(sender='Hola', content='Hula')
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_string_repr_status(self):
        status = Status(sender='Hola', content='Hula')
        self.assertEqual(str(status), status.sender)

class StatusRequestTestCase(TestCase):
    def test_status_add_post(self):
        response_post = Client().post(
            '/confirm', {'sender': 'Haha'})
        self.assertEqual(response_post.status_code, 302)

class StatusFunctionalTestCase(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        cls.selenium = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        cls.selenium.implicitly_wait(25)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_add_status_then_confirm(self):
        self.selenium.get(self.live_server_url)
        self.selenium.find_element_by_id('btn-add').click()

        s = 'Alex'
        c = 'Lorem Ipsum dor sit amet blulbu'

        sender_el = self.selenium.find_element_by_name('sender')
        content_el = self.selenium.find_element_by_name('content') 

        # type the contents
        sender_el.send_keys(s)
        content_el.send_keys(c)

        # off to the confirm page
        self.selenium.find_element_by_id('btn-submit').click()

        form_title = self.selenium.find_elements_by_css_selector('#confirm-form > .title')
        self.assertEqual(form_title[0].text, 'Confirm Message')
    
        # confirm
        self.selenium.find_element_by_id('btn-confirm').click()

        # check that we have our message
        subtitle = self.selenium.find_elements_by_css_selector('h2.subtitle')[0]
        self.assertEqual(subtitle.text, 'all the status!')
        self.assertIn('by ' + s, self.selenium.page_source)
        self.assertIn(c, self.selenium.page_source)

    def test_add_status_then_cancel(self):
        self.selenium.get(self.live_server_url)
        self.selenium.find_element_by_id('btn-add').click()

        s = 'Alex2'
        c = 'Lorem Ipsum dor sit amet blulbu2'

        sender_el = self.selenium.find_element_by_name('sender')
        content_el = self.selenium.find_element_by_name('content') 

        # type the contents
        sender_el.send_keys(s)
        content_el.send_keys(c)

        # off to the confirm page
        self.selenium.find_element_by_id('btn-submit').click()

        form_title = self.selenium.find_elements_by_css_selector('#confirm-form > .title')
        self.assertEqual(form_title[0].text, 'Confirm Message')
    
        # cancel
        self.selenium.find_element_by_id('btn-cancel').click()

        # check that we don't have our message
        subtitle = self.selenium.find_elements_by_css_selector('h2.subtitle')[0]
        self.assertEqual(subtitle.text, 'all the status!')
        self.assertNotIn('by ' + s, self.selenium.page_source)
        self.assertNotIn(c, self.selenium.page_source)

    def test_change_status_color(self):
        Status.objects.create(sender='Hola', content='Hula') 

        self.selenium.get(self.live_server_url)
        first_message_header = self.selenium.find_elements_by_css_selector('.message-header')[0]
        initial_color = first_message_header.value_of_css_property('background-color')

        color_button = first_message_header.find_elements_by_css_selector('.btn-change-color')[0]
        color_button.click()

        after_color = self.selenium.find_elements_by_css_selector('.message-header')[0].value_of_css_property('background-color')

        self.assertNotEqual(initial_color, after_color)
 
 