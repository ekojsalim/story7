from django.db import models

# Create your models here.
class Status(models.Model):
    sender = models.CharField(max_length=200)
    content = models.TextField()
    color = models.CharField(max_length=200, default='dark')

    def __str__(self):
        return self.sender
